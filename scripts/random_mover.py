#!/usr/bin/env python
# license removed for brevity
import rospy
from geometry_msgs.msg import Twist
from turtlesim.srv import *
import random

def talker():
    turtle_1_pub = rospy.Publisher('/turtle1/cmd_vel', Twist, queue_size=10)
    turtle_2_pub = rospy.Publisher('/turtle2/cmd_vel', Twist, queue_size=10)
    turtle_3_pub = rospy.Publisher('/turtle3/cmd_vel', Twist, queue_size=10)
    turtle_4_pub = rospy.Publisher('/turtle4/cmd_vel', Twist, queue_size=10)
    turtle_5_pub = rospy.Publisher('/turtle5/cmd_vel', Twist, queue_size=10)
    turtle_6_pub = rospy.Publisher('/turtle6/cmd_vel', Twist, queue_size=10)
    turtle_7_pub = rospy.Publisher('/turtle7/cmd_vel', Twist, queue_size=10)

    pub_list = [turtle_1_pub, turtle_2_pub, turtle_3_pub, turtle_4_pub, turtle_5_pub, turtle_6_pub, turtle_7_pub]

    rospy.init_node('talker', anonymous=True)
    rospy.loginfo("Type : rosrun tf tf_echo turtle1 turtle2\n")

    r = rospy.Rate(1) # 10hz

    while not rospy.is_shutdown():
        for i in range(0, 7):
            twist_msg = Twist()
            twist_msg.linear.x = random.uniform(-1.0,3.0)
            twist_msg.angular.z = random.uniform(-1.5,1.5)
            pub_list[i].publish(twist_msg)

        r.sleep()

if __name__ == '__main__':
    rospy.wait_for_service('spawn')

    try:
        spawn = rospy.ServiceProxy('spawn', Spawn)
        resp1 = spawn(5.5, 5.5, 0, "turtle2")
        resp1 = spawn(5.5, 5.5, 0, "turtle3")
        resp1 = spawn(5.5, 5.5, 0, "turtle4")
        resp1 = spawn(5.5, 5.5, 0, "turtle5")
        resp1 = spawn(5.5, 5.5, 0, "turtle6")
        resp1 = spawn(5.5, 5.5, 0, "turtle7")
        
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e

    try:
        talker()
    except rospy.ROSInterruptException: pass
