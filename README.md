# turtlesim_mod
[ros_tutorials/turtlesim](https://github.com/ros/ros_tutorials)に機能を追加したもの。

## Install

```bash
$ cd catkin_ws/src
$ git clone https://bitbucket.org/akiHonda/turtlesim_mod.git
$ cd catkin_ws
$ catkin_make
```

## tf2
tf2を生成するようにして、ランダムに動くTurtleの位置を確認できるようにした。

### 起動  
```bash
$ roslaunch turtlesim_mod tf2.launch
```

### TF Treeの確認  
```bash
$ rqt
```
GUIのメニューよりPlugins->Visualization->TF treeでプラグインを選択

### TF機能の確認
```
$ rosrun tf tf_echo turtle1 turtle2
```

## Dynamic Reconfigure
Dynamic reconfigureの機能で背景の色を動的に変更できるようにした。

### 起動
```bash
$ roslaunch turtlesim_mod dynrecnf.launch
```

rqtのsimという項目をクリックすることで設定項目が表示され、背景色のRGBを変更できる。

## PS3コントローラ
説明(TODO)

## State machine (TODO: 調整中)
SMACHの機能を使用し、state machineを組み込んだ。

smach_viewerの導入

Smachで作成した状態機械を視覚的に確認できる、"smach_viewer"を導入します。 ただし、ROS Indigoだと、デフォルトではsmach_viewerがうまく動かないので、簡単な改造をします。

まずは導入。

sudo apt-get install ros-indigo-smach-viewer
そして、改造します。

ROS Indigo :Cannot show Graph View on smach_viewer - ROS Answers: Open Source Q&A Forum

/opt/ros/indigo/lib/python2.7/dist-packages/xdot/xdot.pyの480行目を、以下のように書き換えます

管理者権限が必要なので、sudoでエディタを立ち上げて、書き換えてください。

# return int(self.read_code())     #元々のコード
return int(float(self.read_code())) #書き換えたコード